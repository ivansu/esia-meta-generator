function saveAs(uri, filename) {
    var link = document.createElement('a');
    if (typeof link.download === 'string') {
        document.body.appendChild(link); // Firefox requires the link to be in the body
        link.download = filename;
        link.href = uri;
        link.click();
        document.body.removeChild(link); // remove the link when done
    } else {
        location.replace(uri);
    }
}

(function ( $ ) {
    'use strict';

    $(document).ready(
        function() {
            $('.js-protocol-choice').bind('click', function() {

                var choice = $(this).html();

                $('.js-protocol').text(choice);
                $('.js-protocol-button').val(choice);
            });
            $('#domain').bind('change', function() {

                var domain = this.value;

                $('.js-domain-listener').each(function()  {
                    var value = $(this).val();
                    $(this).val($.trim(domain).concat(value.substr(value.indexOf('/'))))
                });
            });
            $('#meta-generator').validator({
                'custom': {
                    'entity': function($el) {
                        var domainPattern = /^((((?:(?:(?:\w[\.\-\+]?)*)\w)+)((?:(?:(?:\w([\.\-\+]|\-\-)?){0,62})\w)+)\.([\w\-]{2,}))|(((?:\d+\.){3}(?:\d+))))(:\d+)?$/;
                        return ($('button .js-protocol:contains("http")').length) && ($el.val().match(domainPattern) != null);
                    }
                },
                'errors': {
                    'entity': 'Please, choice the protocol'
                }
            });
            $("#meta-generator").submit(function(event) {

                var protocol = $('button .js-protocol').text();
                var formData = {
                    entityID: protocol + $.trim($('#domain').val()),
                    x509Certificate: $('#cert').val().replace(/(-)+BEGIN CERTIFICATE(-)+/g,'').replace(/(-)+END CERTIFICATE(-)+/g,'').replace(/\s/g,''),
                    singleLogoutServiceLocation: protocol + $.trim($('#SingleLogoutService-Location').val()),
                    singleLogoutServiceResponseLocation: protocol + $.trim($('#SingleLogoutService-ResponseLocation').val()),
                    assertionConsumerServiceLocation: protocol + $.trim($('#AssertionConsumerService-Location').val()),
                    organizationName: $.trim($('#OrganizationName').val()),
                    organizationURL: $.trim($('#OrganizationURL').val()),
                    emailAddress: $.trim($('#EmailAddress').val())
                }

                $(this).find('[type="checkbox"]').each(function() {
                    formData[$(this).attr('id')] = $(this).prop("checked");
                });

                $.ajax({
                    url: 'meta-template.xml',
                    type: 'GET',
                    dataType: 'text',
                    error: function(){
                        alert('Error loading XML document');
                    },
                    success: function(xml){
                        var rendered = Mustache.render(xml, formData);
                        var filename = "metadata.xml";
                        var dataUri = "data:application/octet-stream;filename=" + filename + "," + encodeURIComponent(rendered);

                        saveAs(dataUri, filename);
                    }
                });
                event.preventDefault();
            }).on('keyup keypress', function(e) {
                var code = e.keyCode || e.which;
                if (code == 13) {
                    e.preventDefault();
                    return false;
                }
            });
            $('.js-product-button').bind('click', function() {
                var domain = $('#domain').val();
                var locations = {
                    'm3': {
                        'singleLogoutServiceLocation': '/auth/esia/receive-logout-request',
                        'singleLogoutServiceResponseLocation': '/auth/esia/receive-logout-response',
                        'assertionConsumerServiceLocation': '/auth/esia/receive-authn-response'
                    },
                    'd3': {
                        'singleLogoutServiceLocation': '/webservice/esia/logout',
                        'singleLogoutServiceResponseLocation': '/webservice/esia/logout',
                        'assertionConsumerServiceLocation': '/webservice/esia/login'
                    },
                    'other': {
                        'singleLogoutServiceLocation': '',
                        'singleLogoutServiceResponseLocation': '',
                        'assertionConsumerServiceLocation': ''
                    }
                };
                var platform = $(this).attr('data-platform');

                $('.js-domain-listener').each(function()  {

                    var singleLogoutServiceLocation = locations[platform]['singleLogoutServiceLocation'];
                    var singleLogoutServiceResponseLocation = locations[platform]['singleLogoutServiceResponseLocation'];
                    var assertionConsumerServiceLocation = locations[platform]['assertionConsumerServiceLocation'];

                    $('#SingleLogoutService-Location').val($.trim(domain).concat(singleLogoutServiceLocation));
                    $('#SingleLogoutService-ResponseLocation').val($.trim(domain).concat(singleLogoutServiceResponseLocation));
                    $('#AssertionConsumerService-Location').val($.trim(domain).concat(assertionConsumerServiceLocation));
                });
            });
        }
    );
})( jQuery );
